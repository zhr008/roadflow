﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RoadFlow.Business
{
    public class FlowButton
    {
        private readonly Data.FlowButton flowButtonData;
        public FlowButton()
        {
            flowButtonData = new Data.FlowButton();
        }
        /// <summary>
        /// 得到所有按钮
        /// </summary>
        /// <returns></returns>
        public List<Model.FlowButton> GetAll()
        {
            return flowButtonData.GetAll();
        }
        /// <summary>
        /// 得到一个按钮
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Model.FlowButton Get(Guid id)
        {
            return GetAll().Find(p => p.Id == id);
        }
        /// <summary>
        /// 添加一个按钮
        /// </summary>
        /// <param name="flowButton">按钮实体</param>
        /// <returns></returns>
        public int Add(Model.FlowButton flowButton)
        {
            return flowButtonData.Add(flowButton);
        }
        /// <summary>
        /// 更新按钮
        /// </summary>
        /// <param name="flowButton">按钮实体</param>
        public int Update(Model.FlowButton flowButton)
        {
            return flowButtonData.Update(flowButton);
        }
        /// <summary>
        /// 删除按钮
        /// </summary>
        /// <param name="flowButtons">按钮实体</param>
        /// <returns></returns>
        public int Delete(Model.FlowButton[] flowButtons)
        {
            return flowButtonData.Delete(flowButtons);
        }
        /// <summary>
        /// 得到最大序号
        /// </summary>
        /// <returns></returns>
        public int GetMaxSort()
        {
            var all = GetAll();
            return all.Count == 0 ? 5 : all.Max(p => p.Sort) + 5;
        }
    }
}
