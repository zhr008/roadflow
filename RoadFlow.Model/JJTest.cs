﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoadFlow.Model
{
    public class JJTest
    {

        [Key]
        public Guid Id { get; set; }

        [DisplayName("名称")]
        public string Name { get; set; }

        public DateTime? Time1 { get; set; }

        public DateTime? Time2 { get; set; }

        public long? Count { get; set; }

        public string Html { get; set; }
    }
}
