﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FlowArchiveController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            ViewData["flowOptions"] = new Business.Flow().GetOptions();
            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["queryString"] = Request.UrlQuery();
            return View();
        }
        [Validate]
        public string query()
        {
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            string order = (sidx.IsNullOrEmpty() ? "WriteTime" : sidx) + " " + (sord.IsNullOrEmpty() ? "ASC" : sord);

            string FlowID = Request.Forms("FlowID");
            string stepName = Request.Forms("stepName");
            string flowTitle = Request.Forms("flowTitle");
            string Date1 = Request.Forms("Date1");
            string Date2 = Request.Forms("Date2");

            System.Data.DataTable dt = new Business.FlowArchive().GetPagerData(out int count, size, number, FlowID, stepName, flowTitle, Date1, Date2, order);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                string title = "<a class=\"blue\" href=\"javascript:void(0);\" onclick=\"show('" + dr["Id"].ToString()
                    + "', '" + dr["FlowId"].ToString() + "','" + dr["StepId"].ToString() + "','" +
                    dr["TaskId"].ToString() + "','" + dr["GroupId"].ToString() + "','" + dr["InstanceId"].ToString() + "');return false;\">" + dr["Title"].ToString() + "</a>";
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Title", title },
                    { "FlowName", dr["FlowName"].ToString() },
                    { "StepName", dr["StepName"].ToString() },
                    { "WriteTime", dr["WriteTime"].ToString().ToDateTime().ToDateTimeString() },
                    { "UserName", dr["UserName"].ToString() },
                    { "Opation", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"show('" + dr["Id"].ToString()
                    + "', '"+dr["FlowId"].ToString()+"','"+dr["StepId"].ToString()+"','"+
                    dr["TaskId"].ToString()+"','"+dr["GroupId"].ToString()+"','"+dr["InstanceId"].ToString()+"');return false;\"><i class=\"fa fa-search\"></i>查看</a>" }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";

        }
    }
}