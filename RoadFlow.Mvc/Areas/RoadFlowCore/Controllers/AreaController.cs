﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class AreaController : Controller
    {
        public IActionResult Index()
        {

            var items = new Business.Area().GetAll();
            var lists=items.Select(t => new JObject
            {
                {"Id",t.Id },
                {"Name",t.Name },
                {"Opation","" }
            }).ToList();

            ViewData["json"] = Newtonsoft.Json.JsonConvert.SerializeObject(lists);

            return View();
        }


        [Validate]
        public IActionResult Edit()
        {
            string buttonId = Request.Querys("buttonid");
            var area = new Business.Area();
            Model.Area item = null;
            //if (long.TryParse(buttonId,out long bid))
            if(buttonId.IsGuid(out Guid bid))
            {
                item = area.Get(bid);
            }
            if (null == item)
            {
                item = new Model.Area
                {
                    Id = Guid.NewGuid()
                };
            }
            ViewData["queryString"] = Request.UrlQuery();
            return View(item);
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Save(Model.Area item)
        {
            if (!ModelState.IsValid)
            {
                return Tools.GetValidateErrorMessag(ModelState);
            }

            var area = new Business.Area();
            var buttonId = Request.Querys("buttonid");
            //if(long.TryParse(buttonId,out long bid))
            if(buttonId.IsGuid(out Guid bid))
            {
                var oldModel = area.Get(bid);
                string oldJson = oldModel?.ToString();
                area.Update(item);
                Business.Log.Add("修改了地区-" + item.Name, type: Business.Log.Type.其它, oldContents: oldJson,newContents:item.ToString());
            }
            else
            {
                area.Add(item);
                Business.Log.Add("添加了地区-" + item.Name, item.ToString(), Business.Log.Type.其它);
            }
            return "保存成功";
        }


        [Validate]
        [ValidateAntiForgeryToken]
        public string Delete()
        {
            string ids = Request.Forms("ids");
            List<Model.Area> items = new List<Model.Area>();
            Business.Area area = new Business.Area();
            var allButtons = area.GetAll();
            foreach (string id in ids.Split(','))
            {
                //if (!long.TryParse(id,out long bid))
                if (!id.IsGuid(out Guid bid))
                {
                    continue;
                }
                var buttonModel = allButtons.Find(p => p.Id == bid);
                if (null == buttonModel)
                {
                    continue;
                }
                items.Add(buttonModel);
            }
            area.Delete(items.ToArray());
            Business.Log.Add("删除了地区", Newtonsoft.Json.JsonConvert.SerializeObject(items), Business.Log.Type.其它);
            return "删除成功!";
        }
    }
}