﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class HomeSetController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            ViewData["query"] = "appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            return View();
        }
        [Validate]
        [ValidateAntiForgeryToken]
        public string Query()
        {
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");
            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            string order = (sidx.IsNullOrEmpty() ? "Type,Name" : sidx) + " " + (sord.IsNullOrEmpty() ? "ASC" : sord);

            string Name1 = Request.Forms("Name1");
            string Title1 = Request.Forms("Title1");
            string Type = Request.Forms("Type");
            Business.HomeSet homeSet = new Business.HomeSet();
            System.Data.DataTable dt = homeSet.GetPagerData(out int count, size, number, Name1, Title1, Type, order);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            Business.Organize organize = new Business.Organize();
            foreach (System.Data.DataRow dr in dt.Rows)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Name", dr["Name"].ToString() },
                    { "Title", dr["Title"].ToString() },
                    { "Type", dr["Type"].ToString().ToInt()==0?"顶部信息统计": dr["Type"].ToString().ToInt() == 1 ? "左边模块" : "右边模块" },
                    { "DataSourceType", dr["DataSourceType"].ToString().ToInt() == 0 ? "SQL语句" : dr["DataSourceType"].ToString().ToInt() == 1 ? "C#方法" : "URL地址" },
                    { "UseOrganizes", organize.GetNames(dr["UseOrganizes"].ToString()) },
                    { "Note", dr["Note"].ToString() },
                    { "Opation", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"edit('" + dr["Id"].ToString() + "');return false;\"><i class=\"fa fa-edit (alias)\"></i>编辑</a>" }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }

        [Validate]
        public IActionResult Edit()
        {
            string id = Request.Querys("id");
            Business.HomeSet homeSet = new Business.HomeSet();
            Model.HomeSet homeSetModel = null;
            if (id.IsGuid(out Guid Id))
            {
                homeSetModel = homeSet.Get(Id);
            }
            if (null == homeSetModel)
            {
                homeSetModel = new Model.HomeSet
                {
                    Id = Guid.NewGuid(),
                    Type = -1,
                    DataSourceType = -1,
                    Sort = homeSet.GetMaxSort()
                };
            }

            ViewData["dbconnOptions"] = new Business.DbConnection().GetOptions(homeSetModel.DbConnId.ToString());
            ViewData["query"] = "id=" + Request.Querys("id") + "&appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            return View(homeSetModel);
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string EditSave(Model.HomeSet homeSetModel)
        {
            if (!ModelState.IsValid)
            {
                return Tools.GetValidateErrorMessag(ModelState);
            }
            Business.HomeSet homeSet = new Business.HomeSet();
            if (Request.Querys("id").IsGuid(out Guid guid))
            {
                var oldModel = homeSet.Get(guid);
                string oldJSON = null == oldModel ? "" : oldModel.ToString();
                homeSet.Update(homeSetModel);
                Business.Log.Add("修改了首页设置 - " + homeSetModel.Name, type: Business.Log.Type.系统管理, oldContents: oldJSON, newContents: homeSetModel.ToString());
            }
            else
            {
                homeSet.Add(homeSetModel);
                Business.Log.Add("添加了首页设置 - " + homeSetModel.Name, homeSetModel.ToString(), Business.Log.Type.系统管理);
            }
            return "保存成功!";
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Delete()
        {
            string ids = Request.Forms("ids");
            List<Model.HomeSet> homeSets = new List<Model.HomeSet>();
            Business.HomeSet homeSet = new Business.HomeSet();
            foreach (string id in ids.Split(','))
            {
                if (!id.IsGuid(out Guid guid))
                {
                    continue;
                }
                var model = homeSet.Get(guid);
                if (null != model)
                {
                    homeSets.Add(model);
                }
            }
            Business.Log.Add("删除了首页设置", Newtonsoft.Json.JsonConvert.SerializeObject(homeSets), Business.Log.Type.系统管理);
            homeSet.Delete(homeSets.ToArray());
            return "删除成功!";
        }
    }
}