﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class MobileController : Controller
    {
        /// <summary>
        /// 微信登录跳转地址获取用户账号
        /// </summary>
        /// <returns></returns>
        public IActionResult GetUserAccount()
        {
            string code = Request.Querys("code");
            if (code.IsNullOrWhiteSpace())
            {
                return new ContentResult() { Content = "身份验证失败" };
            }
            string account = Business.EnterpriseWeiXin.Common.GetUserAccountByCode(code);
            if (account.IsNullOrWhiteSpace())
            {
                return new ContentResult() { Content = "身份验证失败" };
            }
            var userModel = new Business.User().GetByAccount(account);
            if (null == userModel)
            {
                return new ContentResult() { Content = "没有找到帐号对应的人员" };
            }
            HttpContext.Session.SetString(Business.EnterpriseWeiXin.Common.SessionKey, userModel.Id.ToString());
            string lastUrl = HttpContext.Session.GetString("EnterpriseWeiXin_LastURL");

            //添加到在线用户字典
            string uniqueId = Guid.NewGuid().ToUpperString();
            Model.OnlineUser onlineUserModel = new Model.OnlineUser
            {
                UserId = userModel.Id,
                UserName = userModel.Name,
                UserOrganize = new Business.User().GetOrganizeMainShowHtml(userModel.Id, false),
                IP = Tools.GetIP(),
                LastTime = Current.DateTime,
                LastUrl = lastUrl,
                LoginType = 1,
                UniqueId = uniqueId,
                BrowseAgent = Tools.GetBrowseAgent()
            };
            onlineUserModel.LoginTime = onlineUserModel.LastTime;
            Business.OnlineUser.Add(onlineUserModel);

            if (!lastUrl.IsNullOrWhiteSpace())
            {
                return Redirect(lastUrl);
            }
            return new ContentResult() { Content = "" };
        }

        /// <summary>
        /// 待办列表
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult WaitTask()
        {
            return View();
        }
        /// <summary>
        /// 已办列表
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult CompletedTask()
        {
            return View();
        }
        /// <summary>
        /// 发起流程
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult StartFlow()
        {
            return View();
        }
        /// <summary>
        /// 消息中心
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult Message()
        {
            return View();
        }

        #region 文档中心
        /// <summary>
        /// 文档中心
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult Document()
        {
            string title = "";
            Guid userId = Current.EnterpriseWeiXinUserId;
            Business.Doc doc = new Business.Doc();
            Business.DocDir docDir = new Business.DocDir();
            string order = "Rank,WriteTime DESC";
            List<(string, System.Data.DataTable)> docs = new List<(string, System.Data.DataTable)>();
            //查询未读文档
            var noReadDoc = doc.GetPagerList(out int count, 100000, 1, userId, title, "", "", "", order, 0);
            if (noReadDoc.Rows.Count > 0)
            {
                docs.Add(("未读文档", noReadDoc));
            }

            //查询栏目文档
            var dirs = docDir.GetReadDirs(userId);
            foreach (var dir in dirs)
            {
                System.Data.DataTable dt = doc.GetPagerList(out int count1, 5, 1, userId, title, "'" + dir.Id.ToString() + "'", "", "", order, -1);
                if (dt.Rows.Count > 0)
                {
                    string dirName = docDir.GetAllParentNames(dir.Id, true, false);
                    docs.Add((dirName, dt));
                }
            }

            ViewData["noReadDoc"] = noReadDoc;
            return View(docs);
        }
        /// <summary>
        /// 查看文档
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult DocumentShow()
        {
            Guid userId = Current.EnterpriseWeiXinUserId;
            string docId = Request.Querys("docid");
            if (!docId.IsGuid(out Guid id))
            {
                return new ContentResult() { Content = "ID错误" };
            }
            var docModel = new Business.Doc().Get(id);
            if (null == docModel)
            {
                return new ContentResult() { Content = "未找到文档" };
            }
            //更新阅读次数
            new Business.Doc().UpdateReadCount(docModel);
            //更新状态为已读
            new Business.DocUser().UpdateIsRead(docModel.Id, userId, 1);
            return View(docModel);
        }
        /// <summary>
        /// 栏目列表
        /// </summary>
        /// <returns></returns>
        [Validate(CheckLogin = false, CheckApp = false, CheckUrl = false, CheckEnterPriseWeiXinLogin = true)]
        public IActionResult DocumentDir()
        {
            string dirId = Request.Querys("dirid");
            ViewData["dirid"] = dirId;
            ViewData["Title"] = new Business.DocDir().GetAllParentNames(dirId.ToGuid(), true, false);
            return View();
        }

        #endregion
    }
}