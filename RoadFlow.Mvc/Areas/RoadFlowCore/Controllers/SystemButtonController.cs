﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.Mvc.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class SystemButtonController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            var allButtons = new Business.SystemButton().GetAll();
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            foreach (var button in allButtons)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", button.Id },
                    { "Name", button.Name },
                    { "Ico", button.Ico.IsNullOrWhiteSpace() ? "" : button.Ico.IsFontIco() ? "<i class='fa " + button.Ico + "' style='font-size:14px;'></i>" : "<img src='" + button.Ico + "'/>" },
                    { "Note", button.Note },
                    { "Sort", button.Sort },
                    { "Opation", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"edit('" + button.Id + "');return false;\"><i class=\"fa fa-edit (alias)\"></i>编辑</a>" }
                };
                jArray.Add(jObject);
            }

            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["json"] = jArray.ToString();
            return View();
        }

        [Validate]
        public IActionResult Edit()
        {
            string buttonId = Request.Querys("buttonid");
            Business.SystemButton systemButton = new Business.SystemButton();
            Model.SystemButton systemButtonModel = null;
            if (buttonId.IsGuid(out Guid guid))
            {
                systemButtonModel = systemButton.Get(guid);
            }
            if (null == systemButtonModel)
            {
                systemButtonModel = new Model.SystemButton()
                {
                    Id = Guid.NewGuid(),
                    Sort = systemButton.GetMaxSort()
                };
            }
            ViewData["queryString"] = Request.UrlQuery();
            return View(systemButtonModel);
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Save(Model.SystemButton systemButtonModel)
        {
            if (!ModelState.IsValid)
            {
                return Tools.GetValidateErrorMessag(ModelState);
            }
            Business.SystemButton systemButton = new Business.SystemButton();
            if (Request.Querys("buttonid").IsGuid(out Guid guid))
            {
                var oldModel = systemButton.Get(guid);
                string oldJSON = null == oldModel ? "" : oldModel.ToString();
                systemButton.Update(systemButtonModel);
                Business.Log.Add("修改了系统按钮库-" + systemButtonModel.Name, type: Business.Log.Type.系统管理, oldContents: oldJSON, newContents: systemButtonModel.ToString());
            }
            else
            {
                systemButton.Add(systemButtonModel);
                Business.Log.Add("添加了系统按钮库-" + systemButtonModel.Name, systemButtonModel.ToString(), Business.Log.Type.系统管理);
            }
            return "保存成功!";
        }

        [Validate]
        [ValidateAntiForgeryToken]
        public string Delete()
        {
            string ids = Request.Forms("ids");
            var models = new Business.SystemButton().Delete(ids);
            Business.Log.Add("删除了系统按钮库", Newtonsoft.Json.JsonConvert.SerializeObject(models), Business.Log.Type.系统管理);
            return "删除成功!";
        }
    }
}