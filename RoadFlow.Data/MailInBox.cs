﻿using RoadFlow.Mapper;
using RoadFlow.Utility;
using System;
using System.Data;
using System.Data.Common;

namespace RoadFlow.Data
{
  public class MailInBox
  {
    public RoadFlow.Model.MailInBox Get(Guid id)
    {
      using (DataContext dataContext = new DataContext())
        return dataContext.Find<RoadFlow.Model.MailInBox>((object) id);
    }

    public bool AllNoRead(Guid outBoxId)
    {
      using (DataContext dataContext = new DataContext())
      {
        if (dataContext.GetDataTable("SELECT Id FROM RF_MailInBox WHERE OutBoxId={0} AND IsRead=1", new object[1]
        {
          (object) outBoxId
        }).Rows.Count > 0)
          return false;
        return dataContext.GetDataTable("SELECT Id FROM RF_MailDeletedBox WHERE OutBoxId={0} AND IsRead=1", new object[1]
        {
          (object) outBoxId
        }).Rows.Count == 0;
      }
    }

    public int Add(RoadFlow.Model.MailInBox mailInBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Add<RoadFlow.Model.MailInBox>(mailInBox);
        return dataContext.SaveChanges();
      }
    }

    public int Update(RoadFlow.Model.MailInBox mailInBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Update<RoadFlow.Model.MailInBox>(mailInBox);
        return dataContext.SaveChanges();
      }
    }

    public int UpdateIsRead(Guid id, int status, bool isUpdateDate = false)
    {
      using (DataContext dataContext = new DataContext())
      {
        if (isUpdateDate)
          dataContext.Execute("UPDATE RF_MailInBox SET IsRead={0},ReadDateTime={1} WHERE Id={2}", (object) status, (object) DateExtensions.Now, (object) id);
        else
          dataContext.Execute("UPDATE RF_MailInBox SET IsRead={0} WHERE Id={1}", (object) status, (object) id);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(RoadFlow.Model.MailInBox mailInBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Remove<RoadFlow.Model.MailInBox>(mailInBox);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(Guid id, int status)
    {
      using (DataContext dataContext = new DataContext())
      {
        RoadFlow.Model.MailInBox t = dataContext.Find<RoadFlow.Model.MailInBox>((object) id);
        if (t != null)
        {
          if (status == 0)
            dataContext.Add<RoadFlow.Model.MailDeletedBox>(new RoadFlow.Model.MailDeletedBox()
            {
              Id = t.Id,
              IsRead = t.IsRead,
              OutBoxId = t.OutBoxId,
              ReadDateTime = t.ReadDateTime,
              SendDateTime = t.SendDateTime,
              SendUserId = t.SendUserId,
              Subject = t.Subject,
              SubjectColor = t.SubjectColor,
              UserId = t.UserId
            });
          dataContext.Remove<RoadFlow.Model.MailInBox>(t);
        }
        return dataContext.SaveChanges();
      }
    }

    public DataTable GetPagerList(out int count, int size, int number, Guid currentUserId, string subject, string userId, string date1, string date2, string order)
    {
      using (DataContext dataContext = new DataContext())
      {
        DbconnnectionSql dbconnnectionSql = new DbconnnectionSql(Config.DatabaseType);
        ValueTuple<string, DbParameter[]> mailInBoxSql = dbconnnectionSql.SqlInstance.GetMailInBoxSql(currentUserId, subject, userId, date1, date2);
        string sql = mailInBoxSql.Item1;
        DbParameter[] parameters = mailInBoxSql.Item2;
        string paerSql = dbconnnectionSql.SqlInstance.GetPaerSql(sql, size, number, out count, parameters, order);
        return dataContext.GetDataTable(paerSql, parameters);
      }
    }
  }
}
