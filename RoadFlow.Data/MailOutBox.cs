﻿using RoadFlow.Mapper;
using RoadFlow.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace RoadFlow.Data
{
  public class MailOutBox
  {
    public RoadFlow.Model.MailOutBox Get(Guid id)
    {
      using (DataContext dataContext = new DataContext())
        return dataContext.Find<RoadFlow.Model.MailOutBox>((object) id);
    }

    public int Add(RoadFlow.Model.MailOutBox mailOutBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Add<RoadFlow.Model.MailOutBox>(mailOutBox);
        return dataContext.SaveChanges();
      }
    }

    public int Update(RoadFlow.Model.MailOutBox mailOutBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Update<RoadFlow.Model.MailOutBox>(mailOutBox);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(RoadFlow.Model.MailOutBox mailOutBox)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Remove<RoadFlow.Model.MailOutBox>(mailOutBox);
        return dataContext.SaveChanges();
      }
    }

    public int Delete(Guid id)
    {
      using (DataContext dataContext = new DataContext())
      {
        dataContext.Execute("DELETE RF_MailOutBox WHERE Id={0}", new object[1]
        {
          (object) id
        });
        return dataContext.SaveChanges();
      }
    }

    public int Send(RoadFlow.Model.MailOutBox mailOutBox, RoadFlow.Model.MailContent mailContent, List<RoadFlow.Model.User> receiveUsers, bool isAdd)
    {
      using (DataContext dataContext = new DataContext())
      {
        int num1 = 0;
        int num2;
        if (isAdd)
        {
          num2 = num1 + dataContext.Add<RoadFlow.Model.MailOutBox>(mailOutBox);
        }
        else
        {
          num2 = num1 + dataContext.Update<RoadFlow.Model.MailOutBox>(mailOutBox);
          dataContext.Remove<RoadFlow.Model.MailContent>(mailContent);
        }
        dataContext.Add<RoadFlow.Model.MailContent>(mailContent);
        if (mailOutBox.Status == 1)
        {
          List<RoadFlow.Model.MailInBox> mailInBoxList = new List<RoadFlow.Model.MailInBox>();
          foreach (RoadFlow.Model.User receiveUser in receiveUsers)
          {
            RoadFlow.Model.MailInBox mailInBox = new RoadFlow.Model.MailInBox()
            {
              ContentsId = mailContent.Id,
              Id = Guid.NewGuid(),
              IsRead = 0,
              SendDateTime = mailOutBox.SendDateTime,
              SendUserId = mailOutBox.UserId,
              Subject = mailOutBox.Subject,
              SubjectColor = mailOutBox.SubjectColor,
              UserId = receiveUser.Id,
              OutBoxId = mailOutBox.Id
            };
            mailInBoxList.Add(mailInBox);
          }
          dataContext.AddRange<RoadFlow.Model.MailInBox>((IEnumerable<RoadFlow.Model.MailInBox>) mailInBoxList);
        }
        return dataContext.SaveChanges();
      }
    }

    public bool Withdraw(Guid id)
    {
      using (DataContext dataContext = new DataContext())
      {
        if (dataContext.Find<RoadFlow.Model.MailOutBox>((object) id) == null)
          return false;
        dataContext.Execute("DELETE RF_MailInBox WHERE OutBoxId={0}", new object[1]
        {
          (object) id
        });
        dataContext.Execute("UPDATE RF_MailOutBox SET Status=0 WHERE Id={0}", new object[1]
        {
          (object) id
        });
        dataContext.SaveChanges();
        return true;
      }
    }

    public DataTable GetPagerList(out int count, int size, int number, Guid currentUserId, string subject, string date1, string date2, string order, int status = -1)
    {
      using (DataContext dataContext = new DataContext())
      {
        DbconnnectionSql dbconnnectionSql = new DbconnnectionSql(Config.DatabaseType);
        ValueTuple<string, DbParameter[]> mailOutBoxSql = dbconnnectionSql.SqlInstance.GetMailOutBoxSql(currentUserId, subject, date1, date2, status);
        string sql = mailOutBoxSql.Item1;
        DbParameter[] parameters = mailOutBoxSql.Item2;
        string paerSql = dbconnnectionSql.SqlInstance.GetPaerSql(sql, size, number, out count, parameters, order);
        return dataContext.GetDataTable(paerSql, parameters);
      }
    }
  }
}
