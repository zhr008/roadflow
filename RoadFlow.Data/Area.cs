﻿using RoadFlow.Mapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace RoadFlow.Data
{
    public class Area
    {

        private const string CACHEKEY = "roadflow_cache_area";
        
        public List<Model.Area> GetAll()
        {
            object obj = Cache.IO.Get(CACHEKEY);
            if (obj == null)
            {
                using(var db=new DataContext())
                {
                    var list = db.QueryAll<Model.Area>().OrderBy(t => t.Id).ToList();
                    Cache.IO.Insert(CACHEKEY, list);
                    return list;
                }
            }
            else
            {
                return obj as List<Model.Area>;
            }
        }

        public int Add(Model.Area item)
        {
            ClearCache();
            using(var db=new DataContext())
            {
                db.Add(item);
                return db.SaveChanges();
            }
        }

        public int Update(Model.Area item) {

            ClearCache();
            using(var db=new DataContext())
            {
                db.Update(item);
                return db.SaveChanges();
            }

        }

        public int Delete(Model.Area[] items)
        {
            ClearCache();
            using(var db=new DataContext())
            {
                db.RemoveRange(items);
                return db.SaveChanges();
            }
        }

        public void ClearCache()
        {
            Cache.IO.Remove(CACHEKEY);
        }
    }
}
