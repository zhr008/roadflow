﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RoadFlow.Utility;

namespace RoadFlow.IntegrateCore.Areas.RoadFlowCore.Controllers
{
    [Area("RoadFlowCore")]
    public class FlowDesignerController : Controller
    {
        [Validate]
        public IActionResult Index()
        {
            ViewData["query"] = "appid=" + Request.Querys("appid") + "&tabid=" + Request.Querys("tabid");
            return View();
        }
        [Validate]
        public IActionResult Tree()
        {
            ViewData["rootId"] = new Business.Dictionary().GetIdByCode("system_applibrarytype_flow").ToString();
            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["openerid"] = Request.Querys("openerid");
            return View();
        }
        [Validate]
        public IActionResult List()
        {
            string typeId = Request.Querys("typeid");
            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["typeId"] = typeId;
            ViewData["query"] = "typeid=" + typeId + "&appid=" + Request.Querys("appid");
            return View();
        }
        [Validate]
        public string Query()
        {
            string flow_name = Request.Forms("flow_name");
            string typeid = Request.Forms("typeid");
            string sidx = Request.Forms("sidx");
            string sord = Request.Forms("sord");

            int size = Tools.GetPageSize();
            int number = Tools.GetPageNumber();
            string order = (sidx.IsNullOrEmpty() ? "CreateDate" : sidx) + " " + (sord.IsNullOrEmpty() ? "DESC" : sord);
            if (typeid.IsGuid(out Guid typeId))
            {
                var childsId = new Business.Dictionary().GetAllChildsId(typeId);
                typeid = childsId.JoinSqlIn();
            }
            Business.Flow flow = new Business.Flow();
            var flows = flow.GetPagerList(out int count, size, number, flow.GetManageFlowIds(Current.UserId), flow_name, typeid, order);
            Newtonsoft.Json.Linq.JArray jArray = new Newtonsoft.Json.Linq.JArray();
            Business.User user = new Business.User();
            foreach (System.Data.DataRow dr in flows.Rows)
            {
                Newtonsoft.Json.Linq.JObject jObject = new Newtonsoft.Json.Linq.JObject
                {
                    { "id", dr["Id"].ToString() },
                    { "Name", dr["Name"].ToString() },
                    { "CreateDate", dr["CreateDate"].ToString().ToDateTime().ToDateTimeString() },
                    { "CreateUser", user.GetName(dr["CreateUser"].ToString().ToGuid()) },
                    { "Status", flow.GetStatusTitle(dr["Status"].ToString().ToInt()) },
                    { "Note", dr["Note"].ToString() },
                    { "Opation", "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"openflow('" +dr["Id"].ToString() + "', '" + dr["Name"].ToString() + "');return false;\"><i class=\"fa fa-edit (alias)\"></i>编辑</a>" +
                    "<a class=\"list\" href=\"javascript:void(0);\" onclick=\"delflow('" + dr["Id"].ToString() + "');return false;\"><i class=\"fa fa-remove\"></i>删除</a>" }
                };
                jArray.Add(jObject);
            }
            return "{\"userdata\":{\"total\":" + count + ",\"pagesize\":" + size + ",\"pagenumber\":" + number + "},\"rows\":" + jArray.ToString() + "}";
        }
        [Validate]
        public IActionResult Index1()
        {
            ViewData["appId"] = Request.Querys("appid");
            ViewData["tabId"] = Request.Querys("tabid");
            ViewData["flowId"] = Request.Querys("flowid");
            ViewData["isNewFlow"] = Request.Querys("isnewflow");
            return View();
        }
        /// <summary>
        /// 设置流程属性
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult Set_Flow()
        {
            string flowId = Request.Querys("flowid");
            string isAdd = Request.Querys("isadd");
            if (flowId.IsNullOrWhiteSpace())
            {
                flowId = Guid.NewGuid().ToString();
            }
            ViewData["isAdd"] = isAdd;
            ViewData["openerid"] = Request.Querys("openerid");
            ViewData["flowId"] = flowId;
            ViewData["defaultManager"] = Business.Organize.PREFIX_USER + Current.UserId;
            ViewData["dbconnOptions"] = new Business.DbConnection().GetOptions();
            ViewData["flowTypeOptions"] = new Business.Dictionary().GetOptionsByCode("system_applibrarytype_flow", value: "");
            return View();
        }

        /// <summary>
        /// 设置步骤属性
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult Set_Step()
        {
            ViewData["stepId"] = Request.Querys("id");
            ViewData["x"] = Request.Querys("x");
            ViewData["y"] = Request.Querys("y");
            ViewData["width"] = Request.Querys("width");
            ViewData["height"] = Request.Querys("height");
            ViewData["issubflow"] = Request.Querys("issubflow");
            ViewData["openerid"] = Request.Querys("openerid");
            ViewData["formTypes"] = new Business.Dictionary().GetOptionsByCode("system_applibrarytype");
            ViewData["flowOptions"] = new Business.Flow().GetOptions();
            return View();
        }

        /// <summary>
        /// 设置连线属性
        /// </summary>
        /// <returns></returns>
        [Validate]
        public IActionResult Set_Line()
        {
            ViewData["openerid"] = Request.Querys("openerid");
            ViewData["lineId"] = Request.Querys("lineid");
            ViewData["fromId"] = Request.Querys("fromid");
            ViewData["toId"] = Request.Querys("toid");
            return View();
        }

        /// <summary>
        /// 得到流程JSON
        /// </summary>
        /// <returns></returns>
        [Validate]
        public string GetJSON()
        {
            string flowId = Request.Querys("flowid");
            if (!flowId.IsGuid(out Guid fId))
            {
                return "{}";
            }
            else
            {
                var flow = new Business.Flow().Get(fId);
                return null == flow ? "" : flow.DesignerJSON;
            }
        }

        [Validate]
        public IActionResult Opation()
        {
            string op = Request.Querys("op");
            string msg = string.Empty;
            switch (op)
            {
                case "save":
                    msg = "正在保存...";
                    break;
                case "install":
                    msg = "正在安装...";
                    break;
                case "uninstall":
                    msg = "正在卸载...";
                    break;
                case "delete":
                    msg = "正在删除...";
                    break;
            }
            ViewData["msg"] = msg;
            ViewData["appId"] = Request.Querys("appid");
            ViewData["openerid"] = Request.Querys("openerid");
            ViewData["op"] = op;
            return View();
        }

        /// <summary>
        /// 保存流程
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        public string Save()
        {
            string json = Request.Forms("json");
            string msg = new Business.Flow().Save(json);
            return "1".Equals(msg) ? "保存成功!" : msg;
        }

        /// <summary>
        /// 安装流程
        /// </summary>
        /// <returns></returns>
        public string Install()
        {
            string json = Request.Forms("json");
            string msg = new Business.Flow().Install(json);
            return "1".Equals(msg) ? "安装成功!" : msg;
        }

        /// <summary>
        /// 删除,卸载流程
        /// </summary>
        /// <returns></returns>
        [Validate]
        [ValidateAntiForgeryToken]
        public string UnInstall()
        {
            string flowId = Request.Forms("flowid");
            string status = Request.Forms("status");
            Business.Flow flow = new Business.Flow();
            var flowModel = flow.Get(flowId.ToGuid());
            if (null == flowModel)
            {
                return "未找到流程实体!";
            }
            int status1 = status.ToInt(3);
            Business.Log.Add(("2".Equals(status) ? "卸载" : "删除") + "了流程-" + flowModel.Name, flowModel.ToString(), Business.Log.Type.流程管理);
            flowModel.Status = status1;
            flow.Update(flowModel);
            flow.ClearCache(flowModel.Id);
            return "1";
        }
    }
}