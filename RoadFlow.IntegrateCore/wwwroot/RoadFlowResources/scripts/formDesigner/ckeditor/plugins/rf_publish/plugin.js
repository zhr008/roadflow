CKEDITOR.plugins.add('rf_publish', {
    init: function (editor) {
        var pluginName = 'rf_publish';
        //给自定义插件注册一个调用命令
        editor.addCommand(pluginName, {
            exec: function () {
                var attJSON = formAttributeJSON;
                var eventJSON = formEventsJSON;
                var subtableJSON = formSubtabsJSON;
                var html = ckEditor.getData();
                var formHtml = compileForm(attJSON, eventJSON, subtableJSON, html);
                $.ajax({
                    url: "../FormDesignerPlugin/PublishForm?" + query,
                    type: "post",
                    data: {
                        "attr": JSON.stringify(attJSON),
                        "event": JSON.stringify(eventJSON),
                        "subtable": JSON.stringify(subtableJSON),
                        "html": html,
                        "formHtml": formHtml
                    },
                    success: function (txt) {
                        alert(txt);
                    }
                });
            }
        });
        //注册一个按钮，来调用自定义插件
        editor.ui.addButton(pluginName, {
            label: "发布表单",
            command: pluginName,
            icon: this.path + "publish.png",
            toolbar: 'rf_plugins,21'
        });
    }
});