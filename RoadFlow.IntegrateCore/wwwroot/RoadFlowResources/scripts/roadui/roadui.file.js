﻿//文件上传
; RoadUI.File = function () {
    var instance = this;
    this.init = function ($files) {
        $files.each(function () {
            var $file = $(this);
            var id = $file.attr("id") || "";
            var name = $file.attr("name") || "";
            var filetype = $file.attr("filetype") || "";
            var value = $file.val();
            var validate = $file.attr("validate") || "";
            if (name.length == 0) {
                name = id;
            }
            var $hide = $('<input type="hidden" id="' + id + '" name="' + name + '" value="" ' + (validate && validate.length > 0 ? 'validate="' + validate + '"' : '') + '/>');
            var $but = $('<input type="button" class="mybutton" style="margin:0;" value="附件" />');
            $file.attr("id", id + "_text");
            $file.attr("name", name + "_text");
            $file.attr("readonly", "readonly");
            $file.removeClass().addClass("mytext");
            $file.css({
                "border-right": "0",
                "margin-top": "1px"
            });
            $hide.val(value);

            if (value.length > 0) {

                $file.val('共' + value.split('|').length + '个文件');
            }
            $but.bind("click", function () {
                var val = $(this).prev().prev().val();
                var eid = $(this).prev().prev().attr("id");
                new RoadUI.Window().open({
                    id: "file_" + id,
                    url: "/RoadFlowCore/Controls/UploadFiles_Index?eid=" + eid + "&value=" + encodeURI(val) + "&filetype=" + filetype,
                    width: 780,
                    height: 498,
                    title: "附件",
                    showclose: true
                });
            });
            if (validate && validate.length > 0) {
                $file.removeAttr("validate");
                //$but.after('<span class="msg"></span>');
            }
            $file.after($but).before($hide);
        });
    };
};