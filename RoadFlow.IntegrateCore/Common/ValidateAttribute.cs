﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RoadFlow.Utility;
using System;

namespace RoadFlow.IntegrateCore
{
    /// <summary>
    /// 自定义验证属性类
    /// </summary>
    public class ValidateAttribute : Attribute , IActionFilter
    {
        /// <summary>
        /// 是否验证登录
        /// </summary>
        public bool CheckLogin { get; set; } = true;

        /// <summary>
        /// 是否验证权限
        /// </summary>
        public bool CheckApp { get; set; } = true;

        /// <summary>
        /// 是否验证URL
        /// </summary>
        public bool CheckUrl { get; set; } = true;

        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            Guid userId = Guid.Empty;
            //验证登录
            if (CheckLogin)
            {
                userId = Current.UserId;
                
            }
            //验证菜单权限
            if (CheckApp)
            {
                bool isApp = ValidateApp(context.HttpContext.Request.Querys("appid"), userId);
                if (!isApp)
                {
                    //context.Result = new ContentResult() { Content = "您没有权限使用该功能" };
                    return;
                }
            }
            //验证URL
            if (CheckUrl)
            {
                bool isUrl = ValidateURL(context.HttpContext.Request);
                if (!isUrl)
                {
                    context.Result = new ContentResult() { Content = "URL检查错误" };
                    return;
                }
            }
        }
        /// <summary>
        /// 验证是否有菜单使用权限
        /// </summary>
        /// <param name="appId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool ValidateApp(string appId, Guid userId)
        {
            return true;
        }
        /// <summary>
        /// 验证URL
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private bool ValidateURL(HttpRequest request)
        {
            return true;
        }
        
    }
}
